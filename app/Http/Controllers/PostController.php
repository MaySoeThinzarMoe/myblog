<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Command;
class PostController extends Controller
{

    function __construct()
    {
        $this->middleware('auth',[ 'except'=>['index','show'] ] );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
/*        $posts= Post::all();
*/      $postQuery = Post::query();
        $postQuery ->latest();

        if($cat_id=request('cat_id')){
            $postQuery->where('category_id',$cat_id);
        } 

        $posts = $postQuery->get();
        $categories=Category::all();
        return view('blog',compact('posts','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories=Category::all();

        return view('posts.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate(request(),[
            'title'=>'required|min:5',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'body'=>'required|min:10'
        ]);

        $imageName=time().'.'.request()->photo->getClientOriginalExtension();
        request()->photo->move(public_path('images'),$imageName);

        $fullImg='/images/'.$imageName;

        Post::create([
            'title'=>request('title'),
            'category_id'=>request('categorylist'),
            'photo' =>$fullImg,
            'body'=>request('body'),
            'user_id'=>auth()->id()
        ]);
        return redirect('/');

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $id)
    {
        //
        
        return view('post',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $id)
    {
        //
        $categories=Category::all();
        return view('posts.edit',compact('id','categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate(request(),[
            'title'=>'required|min:5',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'body'=>'required|min:10'
        ]);

        $imageName=time().'.'.request()->photo->getClientOriginalExtension();
        request()->photo->move(public_path('images'),$imageName);

        $fullImg='/images/'.$imageName;

        $id=request('edit');
        $post=Post::find($id);
        $post->title=request('title');
        $post->category_id=request('categorylist');
        $post->photo=$fullImg;
        $post->body=request('body');
        $post->user_id=auth()->id();
        $post->save();

        return redirect('/');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $id)
    {
        //

        foreach($id->comments as $comment):
            $comment->delete();
        endforeach;
        $id->delete();

        return redirect('/')->with('msg','Successfully Delete!');
    }
}
