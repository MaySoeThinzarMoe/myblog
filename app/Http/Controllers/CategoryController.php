<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
class CategoryController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_admin');
/*        $this->middleware('auth',[ 'except'=>['index','show'] ] );
*/    }
    
    public function index()
    {
        //
        $categories= Category::all();
        return view('category',compact('categories'));
    }

    public function create()
    {
        //
        return view('categories.create');
    }

    public function store(Request $request)
    {
        //
        $this->validate(request(),[
            'name'=>'required|min:2',
        ]);

        Category::create([
            'category_name'=>request('name'),
        ]);
        return redirect('/category');
    }

    public function destroy(Category $id)
    {
        //

        foreach($id->posts as $post):
            $post->delete();
        endforeach;

        $id->delete();

        return redirect('/category')->with('msg','Successfully Delete!');
    }


    public function edit(Category $id)
    {
        //
        $categories=Category::all();
        return view('categories.edit',compact('id','categories'));

    }

    public function update(Request $request)
    {
        //
        /*$this->validate(request(),[
            'category_name'=>'required',
        ]);*/

        
        $id=request('edit');
        $category=Category::find($id);
        $category->category_name=request('name');
        $category->save();

        return redirect('/category');

    }
}
