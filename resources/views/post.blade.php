@extends('layouts.template')

@section('content')
<div class="col-lg-8">

          <!-- Title -->
          <h1 class="mt-4">{{ $id->title }}</h1>

          <!-- Author -->
          <p class="lead">
            by
            <a href="#">{{ $id->user->name }}</a>

            @if(Auth::check()&& ($id->user_id== auth()->id()))
            <a href="/post/edit/{{ $id->id }}">Edit</a>
            <a href="/post/delete/{{ $id->id }}">Delete</a>
            @endif
          </p>

          <hr>

          <!-- Date/Time -->
          <p>{{ $id->created_at->toDayDateTimeString()}}</p>

          <hr>

          <!-- Preview Image -->
          <img class="img-fluid rounded" src="{{ $id->photo }}" alt="">

          <hr>

          <!-- Post Content -->
          {{ $id->body }}

          <hr>
          @if(!Auth::check())
           <div class="alert alert-danger">You must login first!</div>
          @endif
          <!-- Comments Form -->
          <div class="card my-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
              <form method="post" action="/comment">
                @csrf
                <input type="hidden" name="postid" value="{{ $id->id }}">
                <div class="form-group">
                  <textarea class="form-control" name="comment" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>

          <!-- Single Comment -->
          @foreach($id->comments as $comment)
           <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="" alt="">
            <div class="media-body">
              <h5 class="mt-0">{{ $comment->user->name }}</h5>
              {{ $comment->body }}
              <br>
              {{ $comment->created_at->diffForHumans()}}
            </div>
          </div>
          @endforeach
</div>
@endsection