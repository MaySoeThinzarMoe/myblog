@extends('layouts.template')

@section('content')
<div class="col-md-8 mt-5">
	@if(count($errors))
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
				<li>{{ $error}}</li>>
			@endforeach
		</ul>
	</div>	
	@endif
	<form action="/category/edit" method="post" enctype="multipart/form-data">
		@csrf
		<input type="hidden" name="edit" value="{{ $id->id }}">
		<div class="form-group">
				<label>Category Name:</label>
				<input type="text" name="name" class="form-control" value="{{ $id->category_name }}">
		</div>
		<input type="submit" name="submit" value="Update" class="btn btn-primary">
	</form>
</div>
@endsection
