@extends('layouts.template')

@section('content')
<div class="col-md-8 mt-5">
	@if(count($errors))
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
				<li>{{ $error}}</li>>
			@endforeach
		</ul>
	</div>	
	@endif
	<form action="/category/create" method="post" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
				<label>Category Name:</label>
				<input type="text" name="name" class="form-control">
		</div>
		<input type="submit" name="submit" value="Add" class="btn btn-primary">
	</form>
</div>
@endsection
