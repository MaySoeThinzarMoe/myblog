@extends('layouts.template')

@section('content')
<div class="col-lg-8 mt-5">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>Category List</h1>
			</div>
			<div class="col-md-4 mr-auto">
				<a class="btn btn-secondary text-white" href="/category/create">Add</a>
			</div>
		</div>
		<div class="row mt-4">
				<table class="table table-bordered">
					<thead class="thead thead-dark">
						<tr>
							<th>No.</th>
							<th>Category Name</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						@foreach($categories as $category)
						<tr>
							<td>{{ $category->id }}</td>
							<td>{{ $category->category_name }}</td>
							<td><a href="/category/edit/{{$category->id}}" class="btn btn-warning">Edit</a></td>
							<td><a href="/category/delete/{{$category->id}}" class="btn btn-danger">Delete</a></td>

						</tr>
						@endforeach
					</tbody>
				</table>
		</div>	 
	</div>
</div>
@endsection