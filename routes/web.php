<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
	return "hello";
    return view('welcome');
});*/

Auth::routes();

Route::get('/','PostController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/post/{id}','PostController@show');

Route::get('admin-login','Auth\AdminLoginController@showLoginForm');
Route::post('admin-login','Auth\AdminLoginController@login');
Route::get('/backend','AdminController@index');

Route::get('/upload','PostController@create');
Route::post('/upload','PostController@store');
Route::get('/post/delete/{id}','PostController@destroy');
Route::get('/post/edit/{id}','PostController@edit');
Route::post('/post/edit','PostController@update');

Route::get('/category','CategoryController@index');
Route::get('/category/create','CategoryController@create');
Route::post('/category/create','CategoryController@store');
Route::get('/category/delete/{id}','CategoryController@destroy');
Route::get('/category/edit/{id}','CategoryController@edit');
Route::post('/category/edit','CategoryController@update');

Route::post('/comment','CommentController@store');